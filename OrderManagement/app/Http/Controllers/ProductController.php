<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = [];
        $products = Product::orderBy('id', 'desc');
       
        $products = $products->paginate(10);
        $data["products"] = $products;

        return view('products.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = [];
        return view('products.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProductRequest $request)
    {
        $dataSave = [
            'name' => $request->name,
            'quantity' => $request->quantity,
            'price' => $request->price,
        ];

        try {
            Product::create($dataSave);
            Log::info('create a Product successfully!');
            return redirect()->route('products.index')-> with('success', 'create successfully');
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return redirect()->route('products.index')-> with('error', 'create failed');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data=[];
        $product = Product::findOrFail($id);
        $data['product'] = $product;

        return view('products.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data=[];
        $product = Product::findOrFail($id);
        $data['product'] = $product;

        return view('products.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProductRequest $request, string $id)
    {
        $product = Product::findOrFail($id);

        $dataUpdate = [
            'name' => $request->name,
            'quantity' => $request->quantity,
            'price' => $request->price,
           
        ];

        try {
            $product->update($dataUpdate);
            Log::info('update successfully!');
            return redirect()->route('products.index')-> with('success', 'update successfully');
        
        } catch (Exception $exception) {

            Log::error($exception->getMessage());
            return redirect()->route('products.index')-> with('error', 'update failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $product = Product::findOrFail($id);
        try {
            Log::info('Delete successfully!');
            $product->delete();
            return redirect()->route('products.index')-> with('success', 'delete successfully');
       } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return redirect()->route('products.index')-> with('error', 'delete failed');
       }
    }
}
