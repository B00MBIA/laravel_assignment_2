<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Order;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = [];
        
        $orders = Order::whereHas('customer', function ($query) {
                $query->where('tel', 'Samsung');
            })
            ->orderBy('id', 'desc')
            ->paginate(10);
        $data["orders"] = $orders;
        
        return view('orders.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = [];
        return view('orders.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreOrderRequest $request)
    {
        $dataSave = [
            'customer_id' => $request->customer_id,
            'date' => $request->date,
        ];
         
        try {
            Order::create($dataSave);
            Log::info('create a Order successfully!');
            return redirect()->route('orders.index')-> with('success', 'create successfully');
        } catch (Exception $exception) {
            Log::error($exception->getMessage());          
            return redirect()->route('orders.index')-> with('error', 'create failed');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data=[];
        $order = Order::findOrFail($id);
        $data['order'] = $order;

        return view('orders.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data=[];
        $order = Order::findOrFail($id);
        $data['order'] = $order;

        return view('orders.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateOrderRequest $request, string $id)
    {
        $order = Order::findOrFail($id);

        $dataUpdate = [
            'customer_id' => $request->customer_id,
            'date' => $request->date,
           
        ];

        try {
            $order->update($dataUpdate);
            Log::info('Update successfully!');
            return redirect()->route('orders.index')-> with('success', 'update successfully');
        } catch (Exception $exception) {

            Log::error($exception->getMessage());
            return redirect()->route('orders.index')-> with('error', 'update failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $order = Order::findOrFail($id);
        try {
            Log::info('Delete successfully!');
            $order->delete();
            return redirect()->route('orders.index')-> with('success', 'delete successfully');
       } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return redirect()->route('orders.index')-> with('error', 'delete failed');
       }
    }
}
