<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCustomerRequest;
use App\Http\Requests\UpdateCustomersRequest;
use App\Models\Customer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = [];
        $customers = Customer::orderBy('id', 'desc');
       
        $customers = $customers->paginate(10);
        $data["customers"] = $customers;

        return view('customers.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = [];
        return view('customers.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCustomerRequest $request)
    {
        $dataSave = [
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            'tel' => $request->tel,
        ];
         
        try {
            Customer::create($dataSave);
            Log::info('create a Customer successfully!');
            return redirect()->route('customers.index')-> with('success', 'create successfully');
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return redirect()->route('customers.index')-> with('error', 'create failed');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data=[];
        $customer = Customer::findOrFail($id);
        $data['customer'] = $customer;

        return view('customers.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data=[];
        $customer = Customer::findOrFail($id);
        $data['customer'] = $customer;

        return view('customers.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCustomersRequest $request, string $id)
    {
        $customer = Customer::findOrFail($id);

        $dataUpdate = [
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            'tel' => $request->tel,
            'birthday' => $request->birthday,
            'gender' => $request->gender,
            'country' => $request->country,
        ];

        try {
            $customer->update($dataUpdate);
            Log::info('update a Customer successfully!');
            return redirect()->route('customers.index')-> with('success', 'update successfully');
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return redirect()->route('customers.index')-> with('error', 'update failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $customer = Customer::findOrFail($id);

        try {
            Log::info('Delete Customer successfully!');
            $customer->delete();
            return redirect()->route('customers.index')-> with('success', 'delete successfully');
       } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return redirect()->route('customers.index')-> with('error', 'delete failed');
       }
    }
}
