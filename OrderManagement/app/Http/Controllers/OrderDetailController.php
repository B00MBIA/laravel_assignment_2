<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderDetailRequest;
use App\Http\Requests\UpdateOrderDetailRequest;
use App\Models\Order;
use App\Models\OrderDetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = [];

        $orders = Order::orderBy('id', 'asc')->paginate(10);
        $OrderDetails = OrderDetail::whereIn('order_id', $orders->pluck('id'))
            ->orderBy('id', 'asc')->paginate(10);

        $data["orders"] = $orders;
        $data["OrderDetails"] = $OrderDetails;
        
        return view('OrderDetails.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = [];
        return view('OrderDetails.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreOrderDetailRequest $request)
    {
        $dataSave = [
            'order_id' => $request->order_id,
            'product_id' => $request->product_id,
            'quantity' => $request->quantity,
            'price' => $request->price,
        ];
         
        try {
            OrderDetail::create($dataSave);
            Log::info('create a Order Detail successfully!');
            return redirect()->route('OrderDetails.index')-> with('success', 'create successfully');
        } catch (Exception $exception) {
            Log::error($exception->getMessage());         
            return redirect()->route('OrderDetails.index')-> with('error', 'create failed');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data=[];
        $OrderDetail = OrderDetail::findOrFail($id);
        $data['OrderDetail'] = $OrderDetail;

        return view('OrderDetails.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data=[];
        $OrderDetail = OrderDetail::findOrFail($id);
        $data['OrderDetail'] = $OrderDetail;

        return view('OrderDetails.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateOrderDetailRequest $request, string $id)
    {
        $OrderDetail = OrderDetail::findOrFail($id);

        $dataUpdate = [
            'order_id' => $request->order_id,
            'product_id' => $request->product_id,
            'quantity' => $request->quantity,
            'price' => $request->price,
        ];

        try {
            $OrderDetail->update($dataUpdate);
            Log::info('update successfully!');
            return redirect()->route('OrderDetails.index')-> with('success', 'update successfully');
        
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return redirect()->route('OrderDetails.index')-> with('error', 'update failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $OrderDetail = OrderDetail::findOrFail($id);
        try {
            Log::info('Delete successfully!');
            $OrderDetail->delete();
            return redirect()->route('OrderDetails.index')-> with('success', 'delete successfully');
       } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return redirect()->route('OrderDetails.index')-> with('error', 'delete failed');
       }
    }
}
