<?php

use App\Http\Controllers\AllOrderController;
use App\Http\Controllers\AllOrderDetailController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\OrderByCurrentMonthController;
use App\Http\Controllers\OrderByCustomerIDController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderDetailController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customers', [CustomerController::class, 'index'])-> name('customers.index');
Route::get('/customers/create', [CustomerController::class, 'create'])-> name('customers.create');
Route::post('/customers', [CustomerController::class, 'store'])-> name('customers.store');
Route::get('/customers/{id}', [CustomerController::class, 'show'])-> name('customers.show');
Route::get('/customers/{id}/edit', [CustomerController::class, 'edit'])-> name('customers.edit');
Route::put('/customers/{id}', [CustomerController::class, 'update'])-> name('customers.update');
Route::delete('/customers/{id}', [CustomerController::class, 'destroy'])-> name('customers.destroy');

Route::get('/orders', [OrderController::class, 'index'])-> name('orders.index');
Route::get('/orders/create', [OrderController::class, 'create'])-> name('orders.create');
Route::post('/orders', [OrderController::class, 'store'])-> name('orders.store');
Route::get('/orders/{id}', [OrderController::class, 'show'])-> name('orders.show');
Route::get('/orders/{id}/edit', [OrderController::class, 'edit'])-> name('orders.edit');
Route::put('/orders/{id}', [OrderController::class, 'update'])-> name('orders.update');
Route::delete('/orders/{id}', [OrderController::class, 'destroy'])-> name('orders.destroy');

Route::get('/products', [ProductController::class, 'index'])-> name('products.index');
Route::get('/products/create', [ProductController::class, 'create'])-> name('products.create');
Route::post('/products', [ProductController::class, 'store'])-> name('products.store');
Route::get('/products/{id}', [ProductController::class, 'show'])-> name('products.show');
Route::get('/products/{id}/edit', [ProductController::class, 'edit'])-> name('products.edit');
Route::put('/products/{id}', [ProductController::class, 'update'])-> name('products.update');
Route::delete('/products/{id}', [ProductController::class, 'destroy'])-> name('products.destroy');

Route::get('/OrderDetails', [OrderDetailController::class, 'index'])-> name('OrderDetails.index');
Route::get('/OrderDetails/create', [OrderDetailController::class, 'create'])-> name('OrderDetails.create');
Route::post('/OrderDetails', [OrderDetailController::class, 'store'])-> name('OrderDetails.store');
Route::get('/OrderDetails/{id}', [OrderDetailController::class, 'show'])-> name('OrderDetails.show');
Route::get('/OrderDetails/{id}/edit', [OrderDetailController::class, 'edit'])-> name('OrderDetails.edit');
Route::put('/OrderDetails/{id}', [OrderDetailController::class, 'update'])-> name('OrderDetails.update');
Route::delete('/OrderDetails/{id}', [OrderDetailController::class, 'destroy'])-> name('OrderDetails.destroy');


Route::get('/allOrder', [AllOrderController::class, 'index'])-> name('allOrder.index');
Route::get('/allOrderDetail', [AllOrderDetailController::class, 'index'])-> name('allOrderDetail.index');
Route::get('/OrderByCustomerID', [OrderByCustomerIDController::class, 'index'])-> name('OrderByCustomerID.index');
Route::get('/OrderByCurrentMonth', [OrderByCurrentMonthController::class, 'index'])-> name('OrderByCurrentMonth.index');