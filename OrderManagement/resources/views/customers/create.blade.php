@extends('layouts.master')

@section('title', 'Create customer list Page')


@section('content')
    <h1>
        Create a customer
    </h1>
    <form action="{{ route('customers.store') }}" method="POST" >
        @csrf

        <div class="form-group">
            <input type="text" name='name' placeholder="name" class="form-control" value="">
            @error('name')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
        <br>
        <div class="form-group">
            <input type="text" name='email' placeholder="email" class="form-control" value="">
            @error('email')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
        <br>
        <div class="form-group">
            <input type="text" name='address' placeholder="address" class="form-control" value="">
            @error('address')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>

        <br>

        <div class="form-group">
            <input type="text" name='phone' placeholder="phone" class="form-control" value="">
            @error('phone')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
        <br>

        <div class="form-group">
            <input type="text" name='tel' placeholder="tel" class="form-control" value="">
            @error('tel')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
        <br>
       


        <br>
        <div class="form-group">
            <a href="{{ route('customers.index') }}" class="btn btn-secondary" >customers list</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    <br> <br><br> <br><br> <br>
@endsection














