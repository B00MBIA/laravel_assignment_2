@extends('layouts.master')

@section('title', 'product list Page')

@section('content')

    {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif
    <a class="btn btn-primary" href="{{ route('products.create') }}">Create product</a>
    <br><br><br>


        @if(!empty($products))
        <table>
           
                <th>ID</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Price</th>
               
                <th colspan="3">Action</th>
                @foreach($products as $product)
                <tr>
                    <td>{{ $product->id }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->quantity }}</td>
                    <td>{{ $product->price }}</td>
                   
                    <td><a href="{{ route('products.show', [ 'id' => $product->id]) }}">Show</a></td>
                    <td><a href="{{ route('products.edit', [ 'id' => $product->id]) }}">Edit</a></td>
                    <td>
                        <form action="{{ route('products.destroy', ['id' => $product->id]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-common" onclick="return confirm('Confirm delete ?')"><i class="fas fa-trash-alt"></i> Delete</button>
                        </form>
                    </td>
                   
                </tr>
            @endforeach
        </table>
        {{$products->appends(request()->input())->links()}}

        @endif
@endsection


<!-- khai báo js chỉ dùng riêng cho trang này -->
@push('js')
    <script src="./js/task.js"></script>
@endpush
<!-- khai báo css chỉ dùng riêng cho trang này -->
@push('css')
    <link rel="stylesheet" href="/css/task.css" >
@endpush








