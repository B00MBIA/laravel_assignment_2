@extends('layouts.master')

@section('title', 'Create customer list Page')


@section('content')
    <h1>
        Create a customer
    </h1>
    <form action="{{ route('products.store') }}" method="POST" >
        @csrf

        <div class="form-group">
            <input type="text" name='name' placeholder="name" class="form-control" value="">
            @error('name')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
        <br>

        <div class="form-group">
            <input type="text" name='quantity' placeholder="quantity" class="form-control" value="">
            @error('quantity')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
        <br>

        <div class="form-group">
            <input type="text" name='price' placeholder="price" class="form-control" value="">
            @error('price')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
        <br>

       

        <br>
        <div class="form-group">
            <a href="{{ route('products.index') }}" class="btn btn-secondary" >product list</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    <br> <br><br> <br><br> <br>
@endsection














