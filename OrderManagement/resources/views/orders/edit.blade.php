@extends('layouts.master')

@section('title', 'Edit order list Page')


@section('content')
    <h1>
        Edit a order
    </h1>
    <form action="{{ route('orders.update', ['id'=> $order->id]) }}" method="POST" >
        @csrf
        @method('PUT')
        <div class="form-group">
            <input type="text" name='customer_id' placeholder="customer_id" class="form-control" value="{{ old('customer_id', $order->customer_id) }}">
            @error('customer_id')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>

        <div class="form-group">
            <input type="text" name='date' placeholder="date" class="form-control" value="{{ old('date', $order->date) }}">
            @error('date')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>

      

        
        <br>
        <div class="form-group">
            <a href="{{ route('orders.index') }}" class="btn btn-secondary" >order list</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    <br> <br><br> <br><br> <br>
@endsection