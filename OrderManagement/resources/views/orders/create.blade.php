@extends('layouts.master')

@section('title', 'Create customer list Page')


@section('content')
    <h1>
        Create a customer
    </h1>
    <form action="{{ route('orders.store') }}" method="POST" >
        @csrf

        <div class="form-group">
            <input type="text" name='customer_id' placeholder="customer_id" class="form-control" value="">
            @error('customer_id')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
        <br>

        <div class="form-group">
            <input type="text" name='date' placeholder="date" class="form-control" value="">
            @error('date')
                <div class="text-danger">{{$message}}</div>
            @enderror
        </div>
       
       


        <br>
        <div class="form-group">
            <a href="{{ route('orders.index') }}" class="btn btn-secondary" >orders list</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
    <br> <br><br> <br><br> <br>
@endsection














