<!DOCTYPE html>
<html>
<head>
<title>@yield('title', 'Home page')</title>
<!-- file css -->
@include('layouts.css')
</head>
<body>
    @include('layouts.header')
    @include('layouts.menu')
    <main>
        <div class="container">
                @yield('content')
        </div>
    </main>
    @include('layouts.footer')
    @include('layouts.js')
</body>
</html>
















