<nav>
    <div class="container">
        <ul>
         
            <li><a href="{{ route('customers.index') }}">Customers</a></li>
            <li><a href="{{ route('products.index') }}">Products</a></li>
            <li><a href="{{ route('allOrder.index') }}">All order</a></li>
            <li><a href="{{ route('allOrderDetail.index') }}">All orderDetail</a></li>
            <li><a href="{{ route('OrderByCustomerID.index') }}">Order by customer id</a></li>
            <li><a href="{{ route('OrderDetails.index') }}">OrderDetails by order id</a></li>
            <li><a href="{{ route('OrderByCurrentMonth.index') }}">Orders by current month</a></li>
            <li><a href="{{ route('orders.index') }}">Orders by phone</a></li>
        </ul>
    </div>
</nav>